## Change the branch to "Master".

## Firabase Auth:
Username: billgates@email.com
password: bill123

## Thanks:
Thanks to everyone at GreenRun for giving me this opportunity, it was very important to me. I'm hoping you guys like what I've developed.

## Development questions:
I couldn't use the API to get the Sports it was giving cross-origin error. But I made a MockService in my React.

## Technologies:
I used ReactQuery to get records from the Api you gave me and from Firebase. In the user section I left it static because I was tired and didn't have time to develop something nicer.
I wanted to use a lot more technologies in my development such as Zustand, to keep the authentication correctly. But because of time I had to be simpler.
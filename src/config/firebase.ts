import config from './config';
import { initializeApp, } from 'firebase/app';
import { getDatabase } from "firebase/database";
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import { getFirestore, where, query, setDoc, doc } from "firebase/firestore";


import { NoteProxy } from '../models/proxies/note.proxy';
import { NotePayload } from '../models/payloads/note.payload';

import { FavoritedProxy } from '../models/proxies/favorited.proxy';
import { FavoritedPayload } from '../models/payloads/favorited.payload';
import { collection, getDocs } from "firebase/firestore";


const app = initializeApp(config.firebase);
const auth = getAuth();
const db = getDatabase(app);
const dbStore = getFirestore();

const refFavorited = collection(dbStore, 'favorited');
const refNotes = collection(dbStore, 'notes');

export const getFavorited = async (uid: string): Promise<FavoritedProxy[]> => {
  const q = query(refFavorited, where("user_id", "==", uid));
  const snapshot = await getDocs(q);
  let result = [];
  snapshot.forEach((doc) => {
    result.push(doc.data());
  });
  return (result as FavoritedProxy[]);
};

export const setFavorited = async (payload: FavoritedPayload) => {
  await setDoc(doc(refFavorited), {
    favorited: payload.favorited,
    sport_id: payload.sport_id,
    user_id: payload.user_id
  });
}

export const getNotes = async (uid: string): Promise<NoteProxy[]> => {
  const q = query(refNotes, where("user_id", "==", uid));
  const snapshot = await getDocs(q);
  let result = [];
  snapshot.forEach((doc) => {
    result.push(doc.data());
  });
  return (result as NoteProxy[]);
};


export const setNote = async (payload: NotePayload) => {
  await setDoc(doc(refNotes), {
    title: payload.title,
    description: payload.description,
    user_id: payload.user_id
  });
}

export const emailAndPassword = (email: string, password: string) => {
  return signInWithEmailAndPassword(auth, email, password);
};

export const database = db;

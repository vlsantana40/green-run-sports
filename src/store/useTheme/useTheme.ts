


export function toggleTheme(): void {
  const storage = localStorage.getItem('theme');
  if (storage) {
    localStorage.setItem('theme', storage === 'light' ? 'dark' : 'light');
    return;
  }

  localStorage.setItem('theme', 'dark');
  return;
};

export function setTheme(themeValue: string): void {
  localStorage.setItem('theme', themeValue);
};

export function getTheme(): string {
  const storage = localStorage.getItem('theme');  
  return storage ? (storage + '') : 'light';
};

export function isLightTheme(): boolean {
  const storage = localStorage.getItem('theme');
  return storage === 'dark';
}

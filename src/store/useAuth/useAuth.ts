
import { FirebaseUser } from '../../models/proxies/firebaseUser.proxy';
// import { UserProxy } from '../../models/proxies/user.proxy';

export function existToken(): boolean {
  const storage = localStorage.getItem('token');
  return storage !== null;
}

export function setToken(tokenValue: string): void {
  localStorage.setItem('token', tokenValue);
}

export function getToken(): string {
  const storage = localStorage.getItem('token');
  return storage ? (storage + '') : '';
}

export function setUser(user: FirebaseUser): void {
  localStorage.setItem('user', JSON.stringify(user));
}

export function getUser(): FirebaseUser | null {
  const storage = localStorage.getItem('user');
  if (storage) {
    try {
      const parsed = JSON.parse(storage);
      return parsed as FirebaseUser;
    } catch (ex) {
      return null;
    }
  }

  return null;
}



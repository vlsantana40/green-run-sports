export function getErrorMessage(error: Error | { message: string | string[] }, fallbackMessage: string = 'Desculpe, ocorreu um erro inesperado.'): string {
  if (!error.hasOwnProperty('message'))
    return fallbackMessage;

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const errorFromResponse = error?.response?.data?.message;

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const errorFromException = error?.response?.data?.exception?.message;

  if (errorFromResponse){
    if (Array.isArray(errorFromResponse))
      return errorFromResponse[0];

    return errorFromResponse;
  }

  if (errorFromException)
    return errorFromException;

  if (Array.isArray(error?.message))
    return error.message[0] || fallbackMessage;

  return error?.message || fallbackMessage;
}

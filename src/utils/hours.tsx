/**
 * função para formatar o payload de tempo do TimePicker
 * @param time o formato do horário HH:mm
 */
export function formatTimePayload(time: string): string {
  let hour = time.split(':')[0];
  let minute = time.split(':')[1];

  hour = (+hour < 10) ? '0' + hour : hour;
  minute = (+minute < 10) ? '0' + minute : minute;

  return `${hour}:${minute}`;
}

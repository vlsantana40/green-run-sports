import moment from 'moment';

/**
 * Get date by string formatted in yyyy-MM-dd
 * @param dateInString
 */
export function getDateFromFormattedString(dateInString: string): Date {
  return moment(dateInString).toDate();
}

import api from '../services/api';
import { getErrorMessage } from './query';

export async function getMany<T>(apiPath: string): Promise<T[]> {
  try {
    const result = await api.get<T[]>(apiPath);

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    return Array.isArray(result.data) ? result.data : result.data.data;
  } catch (error) {
    throw new Error(getErrorMessage(error));
  }
}

export async function getOne<T>(apiPath: string): Promise<T> {
  try {
    const result = await api.get<T>(apiPath);

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    return Array.isArray(result.data) ? result.data : result.data.data;
  } catch (error) {
    throw new Error(getErrorMessage(error));
  }
}

export async function post<T>(apiPath: string, payload): Promise<T> {
  try {
    const result = await api.post<T>(apiPath, payload);

    return result.data;

  } catch (error) {
    throw new Error(getErrorMessage(error));
  }
}

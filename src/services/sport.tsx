import { environment } from '../environments/environment';
import { getMany } from '../utils/api-requests';
import { getErrorMessage } from '../utils/query';
// import api from './api';
import { SportProxy } from '../models/proxies/sport.proxy';


/**
 * @throws { Error }
 */
export async function getAllSports(): Promise<SportProxy[]> {
  try {
    return await getMany<SportProxy>(environment.api.sport.base);
  } catch (error) {
    throw new Error(getErrorMessage(error, 'Houve um problema ao obter as requisições. Tente novamente'));
  }
}


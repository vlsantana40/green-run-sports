import { Theme } from '@emotion/react';
import { getTheme } from './store/useTheme/useTheme';

const isThemeDark: boolean = (getTheme() === 'dark');

console.log('isThemeDark', isThemeDark);

export const theme: Theme = {
    fontBase: `'DM Sans', sans-serif, ${isThemeDark}`,
    fontHeading: `'PT Sans', sans-serif, ${isThemeDark}`,

    colorBody: isThemeDark ? `#181828` : `#f3f3f3`,
    colorPrimary: isThemeDark ? `#236BFE` : `#236BFE`,
    colorPrimaryB: isThemeDark ? `#0D4ED3` : `#0D4ED3`,
    colorSecondary: isThemeDark ? `#2c2b3e` : `#ffffff`,
    colorParag: isThemeDark ? `#fefefe` : `#161617`,
    colorSubTitle: isThemeDark ? `#bfbfc4` : `#4d4c59`,
    colorLabel: isThemeDark ? `#93939e` : `#8a8a8a`,
    colorDanger: isThemeDark ? `#FFF` : `#D36060`,
    colorFavorited: isThemeDark ? `#FFF` : `#2067F8`,
};





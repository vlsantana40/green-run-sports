
import React, { ReactElement } from 'react';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import AppRouting from './App.routing';
import { BrowserRouter } from 'react-router-dom';
import { theme } from './App.theming';
import { ThemeProvider } from 'styled-components';

export const defaultQueryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
    },
  },
});

function App(): ReactElement | null {
  return (
    <ThemeProvider theme={theme}>
      <QueryClientProvider client={defaultQueryClient}>
        <BrowserRouter>
          <AppRouting />
        </BrowserRouter>
        <ReactQueryDevtools initialIsOpen={false} />
      </QueryClientProvider>
    </ThemeProvider>
  );
}

export default App;

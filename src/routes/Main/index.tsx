import { ReactElement } from 'react';
import { Redirect, Route, Switch, useRouteMatch } from 'react-router-dom';

import { Pick } from '../../pages/Pick';
import { History } from '../../pages/History';
import { Notes } from '../../pages/Notes';
import { User } from '../../pages/User';

import Navigator from '../../components/Navigator';

export function Main(): ReactElement {
  const { path, url } = useRouteMatch();


  return (
    <>
      <Switch>
        <Route exact path={`${path}/pick`} component={Pick} />
        <Route exact path={`${path}/history`} component={History} />
        <Route exact path={`${path}/notes`} component={Notes} />
        <Route exact path={`${path}/user`} component={User} />

        <Route exact path="/"><Redirect to={`${url}/pick`} /></Route>
      </Switch>

      <Navigator></Navigator>
    </>
  );
}

import React, { ReactElement, Suspense } from 'react';
import { Switch } from 'react-router-dom';
import PublicRoute from './components/PublicRoute';
import PrivateRoute from './components/PrivateRoute';
import BodyWrapper from './components/BodyWrapper';

import { Home } from './pages/Home';
import { Main } from './routes/Main';


function AppRouting(): ReactElement | null {

    return (
        <BodyWrapper>
            <Suspense fallback={<p>Carregando...</p>}>
                <Switch>
                    <PublicRoute exact path="/">
                        <Home />
                    </PublicRoute>

                    <PrivateRoute path="/main">
                        <Main />
                    </PrivateRoute>
                </Switch>
            </Suspense>
        </BodyWrapper>
    );
}

export default AppRouting;

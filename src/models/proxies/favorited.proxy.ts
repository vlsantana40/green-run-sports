

export interface FavoritedProxy {
    sport_id: number;
    user_id: string;
    favorited: boolean;
}

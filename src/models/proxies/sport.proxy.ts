

export interface SportProxy {    
    idSport: string,
    strSport: string,
    strFormat: string,
    strSportThumb: string,
    strSportIconGreen: string,
    strSportDescription: string,
}

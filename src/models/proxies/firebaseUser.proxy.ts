

export interface FirebaseUser {
    email: string;
    photoURL: string;
    uid: string;
    phoneNumber: string;
    displayName: string;
    providerId: string;
}

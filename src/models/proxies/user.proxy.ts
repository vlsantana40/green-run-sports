//#region Imports

import { BaseCrudProxy } from './base/base.proxy';

//#endregion

/**
 * A interface que representa as informações de um usuário
 */
export interface UserProxy extends BaseCrudProxy {
  roles: string;
  name: string;
  username: string;
  email: string;
  photo?: string;
}

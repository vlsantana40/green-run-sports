

export interface NoteProxy {    
    user_id: string;
    title: string;
    description: string;
}

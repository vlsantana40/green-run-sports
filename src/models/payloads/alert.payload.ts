export interface AlertPayload {
    message: string;
    kind: 'success' | 'warning' | 'danger';
}

export interface FavoritedPayload {
    sport_id: number;
    user_id: string;
    favorited: boolean;
}
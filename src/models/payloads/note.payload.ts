export interface NotePayload {
    user_id: string;
    title: string;
    description: string;
}
export const environment = {
  defaultUnprotectedRoute: '/',
  defaultProtectedRoute: '/main',
  enablePWA: false,
  zustandStoreVersion: 1,
  keys: {
    stayLogged: 'stayLogged',
    laidoffJustification: 'laidoffJustification',
    newSignature: 'newSignature',
    staffRequisition: 'staffRequisition',
  },
  api: {
    baseUrl: 'https://www.thesportsdb.com/api/v1/json/2/all_sports.php',
    auth: {
      local: '/auth/local',
      refresh: '/auth/refresh',
    },
    user: {
      me: '/users/me',
    },
    sport: {
      base: 'all_sports.php',     
    },    
  },
};

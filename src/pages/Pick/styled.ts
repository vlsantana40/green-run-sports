import styled from 'styled-components';


export const SvgWrap = styled.div`
>svg>path {
    fill: ${props => props.theme.colorDanger}
}
`;

export const WrapButtons = styled.div`
display: flex;
justify-content:center;
align-items:center;
height: 30vh;

>div:nth-child(1) {
    margin-right: 18px;
}
`;

export const ImageContainer = styled.div`
background-size: cover;
background-position: center;
background-repeat: no-repeat;
position: relative;
height: 60vh;
width: 100%;
border-radius: 0px 0px 32px 32px;
display:flex;
justify-content:space-between;
flex-direction: column;    
overflow: hidden;

>p {
    width: 100%;
    padding: 28px 20px 20px;
    display: inline-block;        
    font-size: 34px;
    background: linear-gradient(360deg, #000000 0%, #000000 58.85%, rgba(0, 0, 0, 0) 100%);
    font-weight: 700;
    color: #fff;
    font-family: ${props => props.theme.fontBase}
}

>div {
    padding: 16px;
    display: flex;
    justify-content: space-between;
}
`;

export const WrapperContainer = styled.div`
overflow: hidden;
width: 100%;    
height: 100vh;

>div {
    position: absolute;
    width: 100%;        
    height: 100vh;
    will-change: transform;
    touch-action: none;        

    >div {
        width: 100%;            
        height: 100vh;
        will-change: transform;            
        box-shadow: 0 12.5px 100px -10px rgba(50, 50, 73, 0.4), 0 10px 10px -10px rgba(50, 50, 73, 0.3);
    }
}
`;

export const ContainerAnimation = styled.div`
background-color: ${props => props.theme.colorBody};
display: inline-block;
width: 100%;
height: 100vh;
`;

interface ButtonProps {
glassMode?: boolean;
}
export const Button = styled.button<ButtonProps>`
border: none;
padding: 11px 14px;
border-radius: 18px;
${props => props.glassMode ? `background: rgba(34, 34, 67, 0.2);backdrop-filter: blur(20px);` : `background-color: ${props.theme.colorSecondary};`}
transition: .9s ease;

>img {
    display: inline-block;
    width: 26px;
}

&:focus {
    box-shadow: 0px 10px 25px ${props => props.theme.colorPrimary}94;
}
`;


interface AnimationScreenProps {
favorited?: boolean;
}
export const AnimationScreen = styled.div<AnimationScreenProps>`    
position: absolute;
left: 0px;
top: 0px;

display: flex;
justify-content: center;
align-items: center;

width:100%;
height: 100vh;
z-index: 100;

>span:nth-child(1) {
    position: absolute;
    top: 0px;
    left: 0px;
    right: 0px;
    bottom: 0px;
    margin: auto;
    width: 350px;
    height: 350px;
    display: inline-block;
    background-color: #18182857;
    border-radius: 100%;
}
>span:nth-child(2) {
    position: absolute;
    top: 0px;
    left: 0px;
    right: 0px;
    bottom: 0px;
    margin: auto;
    width: 250px;
    height: 250px;
    display: inline-block;
    background-color: #181828CC;
    border-radius: 100%;
}
>span:nth-child(3) {
    position: absolute;
    top: 0px;
    left: 0px;
    right: 0px;
    bottom: 0px;
    margin: auto;
    width: 150px;
    height: 150px;
    display: inline-block;
    background-color: ${props => props.favorited ? props.theme.colorPrimary : props.theme.colorSecondary};
    border-radius: 100%;
    display: flex;
    align-items: center;
    justify-content: center;

    >svg {
        width: 50px;
        height: 50px;
    }
}
`;


import { ReactElement, useState } from 'react';
import ButtonRounded from '../../components/ButtonRounded';
import DarkIcon from '../../assets/icons/dark.svg';
import LightIcon from '../../assets/icons/light.svg';
import { SportProxy } from '../../models/proxies/sport.proxy';
import { isLightTheme, toggleTheme } from '../../store/useTheme/useTheme';
import { getUser } from '../../store/useAuth/useAuth';
import { useSprings, animated, to as interpolate } from 'react-spring'
import { useDrag } from 'react-use-gesture';
import { getFavorited, setFavorited } from '../../config/firebase';
import { useQuery } from 'react-query';
import { FavoritedProxy } from '../../models/proxies/favorited.proxy';
import { AnimPickEnum } from '../../models/enums/anim.enum';
import { mockSports } from '../../services/mockSport';
import { SvgWrap, WrapButtons, ImageContainer, WrapperContainer, ContainerAnimation, Button, AnimationScreen } from './styled';
import { HeartIcon, TimesIcon } from '../../components/Icons';

interface Paginate {
    limit: number,
    offset: number
};

const to = (i) => ({ x: 0, y: i * -4, scale: 1, rot: -10 + Math.random() * 20, delay: i * 100 });
const from = (i) => ({ x: 0, rot: 0, scale: 1.5, y: -1000 });
const trans = (r, s) => `none`;

export function Pick(): ReactElement {

    // is giving cross-origin error
    // const { isLoading: isSportsLoading, data: sportsResponseList } = useQuery(
    //     ['sports'],
    //     async () => await getAllSports(),
    //     {
    //         onSuccess: data => { console.log(data); },
    //         onError: err => console.log(err),
    //     },
    // );
    // is giving cross-origin error    

    const [sportList, setSportList] = useState<SportProxy[]>(mockSports);

    useQuery(
        ['favorited'],
        async () => await getFavorited(getUser().uid),
        {
            onSuccess: (data: FavoritedProxy[]) => {
                const filtered = sportList.filter(({ idSport: id1 }) => !data.some(({ sport_id: id2 }) => id2 === Number(id1)));
                setSportList(filtered);
            },
            onError: err => console.log(err),
        },
    );

    const [anim, setAnim] = useState<Number>(AnimPickEnum.HIDE);
    const [lazy, setLazy] = useState<Paginate>({ limit: 3, offset: 0 });

    const [gone] = useState(() => new Set());
    const [props, set] = useSprings(sportList.length, (i) => ({ ...to(i), from: from(i) }));

    const bind = useDrag(({ args: [index], down, movement: [mx], direction: [xDir], velocity }) => {
        const trigger = velocity > 0.2;
        const dir = xDir < 0 ? -1 : 1;
        if (!down && trigger) gone.add(index);

        set((i) => {
            if (index !== i) return;

            const isGone = gone.has(index);
            let x: number = 0;

            if (isGone) {
                x = (400 + window.innerWidth) * dir;
                if (dir > 0) {
                    liked(sportList[i]);
                } else {
                    notLiked(sportList[i]);
                }
            } else {
                x = down ? mx : 0;
            }

            const rot = mx / 100 + (isGone ? dir * 10 * velocity : 0);
            const scale = down ? 1.1 : 1;

            return { x, rot, scale, delay: undefined, config: { friction: 50, tension: down ? 800 : isGone ? 200 : 500 } };
        });
    });

    const liked = (sport: SportProxy) => {
        setAnim(AnimPickEnum.LIKED);
        setFavorited({
            favorited: true,
            sport_id: Number(sport.idSport),
            user_id: getUser().uid
        });
        nextPage();
    }

    const notLiked = (sport: SportProxy) => {
        setAnim(AnimPickEnum.NOTLIKED);
        setFavorited({
            favorited: false,
            sport_id: Number(sport.idSport),
            user_id: getUser().uid
        });
        nextPage();
    }

    return (
        <>
            {
                anim !== AnimPickEnum.HIDE &&
                <AnimationScreen favorited={anim === AnimPickEnum.LIKED}>
                    <span className="scale-in-center-1s"></span>
                    <span className="scale-in-center-3s"></span>
                    <span className="scale-in-center-5s">
                        {
                            anim === AnimPickEnum.LIKED ? (
                                <HeartIcon></HeartIcon>
                            ) : (
                                <TimesIcon></TimesIcon>
                            )
                        }
                    </span>
                </AnimationScreen>
            }

            <WrapperContainer>
                {
                    props.map(({ x, y, rot, scale }, i) => i > sportList.length - 1 - lazy.limit && i < sportList.length - lazy.offset ? (

                        <animated.div key={i} style={{ x }}>
                            <animated.div {...bind(i)} style={{ transform: interpolate([rot, scale], trans), position: 'relative', zIndex: (i + 2) }}>
                                <ContainerAnimation>
                                    <ImageContainer title={i + ''} style={{ backgroundImage: `url(${sportList[i].strSportThumb})` }}>
                                        <div>
                                            <Button type="button">
                                                <img src={isLightTheme() ? LightIcon : DarkIcon} onClick={() => changeCurrentTheme()} alt="darkmode"></img>
                                            </Button>
                                            <Button type="button" glassMode={true}>
                                                <img src={sportList[i].strSportIconGreen} alt="darkmode"></img>
                                            </Button>
                                        </div>
                                        <p>{sportList[i].strSport}</p>
                                    </ImageContainer>
                                    <WrapButtons>
                                        <div>
                                            <ButtonRounded
                                                width={'51px'}
                                                onClick={() => notLiked(sportList[i])}
                                                primary={false}
                                                padding={'14px 18px'}
                                            >
                                                <SvgWrap>
                                                    <TimesIcon></TimesIcon>
                                                </SvgWrap>
                                            </ButtonRounded>
                                        </div>
                                        <div>
                                            <ButtonRounded
                                                width={'51px'}
                                                onClick={() => liked(sportList[i])}
                                                primary={true}
                                                padding={'21px 20px 16px'}
                                            >
                                                <div>
                                                    <HeartIcon></HeartIcon>
                                                </div>
                                            </ButtonRounded>
                                        </div>
                                    </WrapButtons>
                                </ContainerAnimation>
                            </animated.div>
                        </animated.div>
                    ) : null)
                }
            </WrapperContainer>
        </>
    )

    function changeCurrentTheme(): void {
        toggleTheme();
        setTimeout(() => window.location.reload(), 500);
    }

    function nextPage(): void {
        setTimeout(() => {
            setLazy({ limit: lazy.limit + 1, offset: lazy.offset + 1 });
            setAnim(AnimPickEnum.HIDE);
        }, 1000);
    }
}
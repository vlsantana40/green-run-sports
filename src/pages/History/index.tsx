import { ReactElement, useState } from 'react';
import { useQuery } from 'react-query';

import { getFavorited } from '../../config/firebase';
import { FavoritedProxy } from '../../models/proxies/favorited.proxy';
import { getUser } from '../../store/useAuth/useAuth';
import { mockSports } from '../../services/mockSport';
import { useHistory } from 'react-router-dom';

import { Heading, Wrapper, WrapperList, Parag, ButtonBack, Card } from './styled';
import { ArrowLeftIcon, HeartIcon, TimesIcon } from '../../components/Icons';

interface SportProxyFavorited {
  name: string;
  thumbUrl: string;
  isFavorited: boolean;
}

export function History(): ReactElement {

  const [favoritedList, setFavoritedList] = useState<SportProxyFavorited[]>([]);
  const history = useHistory();

  useQuery(
    ['favorited'],
    async () => await getFavorited(getUser().uid),
    {
      onSuccess: (data: FavoritedProxy[]) => {

        let result: SportProxyFavorited[] = [];

        for (let i = 0; i < data.length; i++) {
          const found = mockSports.find((item) => Number(item.idSport) === data[i].sport_id)
          if (found) {
            result.push({
              name: found.strSport,
              thumbUrl: found.strSportThumb,
              isFavorited: data[i].favorited,
            });
          }
        }

        setFavoritedList(result);
      },
      onError: err => console.log(err),
    },
  );


  return (
    <>
      <Wrapper>
        <ButtonBack type="button" onClick={() => history.push('/main/pick')}>
          <ArrowLeftIcon></ArrowLeftIcon>
        </ButtonBack>
        <Heading> History </Heading>
        <Parag>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        </Parag>
      </Wrapper>
      <WrapperList>
        {
          favoritedList.map((item: SportProxyFavorited, index: number) =>
            <Card key={index} favorited={item.isFavorited}>
              <div style={{ backgroundImage: `url(${item.thumbUrl})` }}>
                <div></div>
                <p>{item.name}</p>
              </div>
              <div>
                <button type="button">
                  {
                    item.isFavorited ? (
                      <HeartIcon></HeartIcon>
                    ) : (
                      <TimesIcon></TimesIcon>
                    )
                  }
                </button>
              </div>
            </Card>
          )
        }
      </WrapperList>
    </>
  )
};

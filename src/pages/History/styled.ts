

import styled from 'styled-components';

interface HeadingProps {
    size?: string;
}
export const Heading = styled.p<HeadingProps>`
        font-size: ${props => props.size ? props.size : '42px'};
        font-weight: 600;
        margin: 10px 0px 10px;
        color: ${props => props.theme.colorParag};
        font-family: ${props => props.theme.fontBase}
    `;
// export const Small = styled.div`
//   margin-bottom: 10px;
//   font-size: 14px;
//   font-weight: 600;
//   color: ${props => props.theme.colorSubTitle};
//   font-family: ${props => props.theme.fontBase}
// `;
export const Wrapper = styled.div`
      width: calc(100% - 70px);        
      margin-left: 35px;
      margin-right: 35px;
      padding: 10px 0px;
      letter-spacing: .5px;
  `;
export const WrapperList = styled.div`
    width: 100%;        
    padding-left: 35px;
    padding-right: 35px;
    max-height: calc(100vh - 285px);
    overflow-y: auto;
  `;

export const Parag = styled.p`
      font-size: 18px;
      font-weight: 400;
      margin-bottom: 10px;
      color: ${props => props.theme.colorSubTitle};
      font-family: ${props => props.theme.fontBase};
    `;

export const ButtonBack = styled.button`
    
      background-color: transparent;
      border: none;
      outline: none;
      &:focus {
        outline: none;
      }
  
      >svg {
        width: 30px;
        height: 30px;      
        path {
          stroke: ${props => props.theme.colorParag};
        }
      }
    
  `;

interface CardProps {
    favorited?: boolean;
}
export const Card = styled.div<CardProps>`
      background-color: ${props => props.theme.colorSecondary};
      border-radius: 12px;
      overflow: hidden;
      margin-bottom: 12px;
      display: flex;
      align-items:center;
      width: 100%;
  
      >div:nth-child(1) {
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        border-radius: 12px;
        width: 75%;
        height: 75px;
        display: flex;
        align-items: center;
        padding: 0px 7px;
        position: relative;
  
        >div {
          position: absolute;
          left: 0px;
          top: 0px;
          width: 100%;
          height: 100%;
          background-color: #000;
          opacity: .5;
        }
  
        >p {
          font-size: 20px;
          font-weight: 700;
          color: #fff;
          font-family: ${props => props.theme.fontBase};
          position: relative;
          z-index: 3;
        }
      }
  
      >div:nth-child(2) {
        width: 25%;
        text-align:center;
  
        >button {
          background-color: transparent;
          border: none;        
  
          &:focus {
            outline: none;
          }
  
          >svg {
            width: 23px;
            height: 23px;
            >path {
              fill: ${props => props.favorited ? props.theme.colorFavorited : '#EA596F'}
            }
          }
        }
      }
  `;


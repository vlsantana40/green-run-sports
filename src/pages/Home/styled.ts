
import styled from 'styled-components';


interface HeadingProps {
  size?: string;
}

export const Heading = styled.p<HeadingProps>`
      font-size: ${props => props.size ? props.size : '42px'};
      font-weight: 700;
      margin-bottom: 5px;
      color: ${props => props.theme.colorParag};
      font-family: ${props => props.theme.fontBase}
  `;

interface WrapperProps {
  constrain?: boolean;
  flex?: boolean;
}
export const Wrapper = styled.div<WrapperProps>`
     width: 100%;
     ${props => props.flex ? ` min-height: 100vh;
     display: flex;
     align-items:center;
     justify-content: center;` : ''}
     ${props => props.constrain ? 'height: 100vh;overflow:hidden;' : 'min-height: 100vh;'}
  
     >div {
       width: 100%;
     }
  `;

export const Parag = styled.p`
    font-size: 18px;
    font-weight: 400;
    color: ${props => props.theme.colorSubTitle};
    font-family: ${props => props.theme.fontBase}
  `;

export const FormWrap = styled.div`
    width: calc(100% - 70px);
    margin-left: 35px;
    margin-right: 35px;
    margin-top: 10px;
  `;

export const Small = styled.div`
    margin: 25px 0px;
    font-size: 16px;
    font-weight: 400;
    color: ${props => props.theme.colorSubTitle};
    font-family: ${props => props.theme.fontBase}
  `;

export const ImageIntro = styled.img`
    max-width: 400px;
    width: 100%;
    min-height: 100vh;
    display: inline-block;
  `;

export const ModalDiv = styled.div`
     background-color:  ${props => props.theme.colorSecondary};
     border-radius: 36px 36px 0px 0px;
     box-shadow: 2px 1px 8px 0px rgba(0,0,0,0.2);
     position: absolute;
     z-index: 4;
     bottom: 0px;
     left: 0px;
     padding: 36px;
  `;

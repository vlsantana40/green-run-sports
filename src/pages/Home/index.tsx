import { ReactElement, useState } from 'react';

import { LoginPayload } from '../../models/payloads/login.payload';
import { AlertPayload } from '../../models/payloads/alert.payload';
import { setToken, setUser } from '../../store/useAuth/useAuth';
import { useHistory } from 'react-router-dom';
import { FirebaseUser } from '../../models/proxies/firebaseUser.proxy';
import FieldText from '../../components/FieldText';
import Button from '../../components/Button';
import Alert from '../../components/Alert';

import { Heading, Wrapper, Parag, FormWrap, Small, ImageIntro, ModalDiv } from './styled';

import { emailAndPassword } from '../../config/firebase';
import MessiIntroImage from '../../assets/images/messi-intro.png';

export function Home(): ReactElement {

  const history = useHistory();

  const [displayLogin, setDisplayLogin] = useState<boolean>(false);
  const [login, setLogin] = useState<LoginPayload>({ username: '', password: '' });
  const [messages, setMessages] = useState<AlertPayload[]>([]);
  const [submited, setSubmited] = useState<boolean>(false);

  return displayLogin ? (
    <>
      <Alert messages={messages} changed={setMessages}></Alert>
      <Wrapper flex>
        <div>
          <div style={{ textAlign: 'center' }}>
            <Heading>Wellcome</Heading>
            <Parag>Lorem ipsum dolor sit amet, <br /> consectetur adipiscing elit.</Parag>
          </div>
          <FormWrap>
            <FieldText
              label={'User'}
              value={login.username}
              onChangeValue={(username) => setLogin({ ...login, username })}
              disabled={submited}
            ></FieldText>
          </FormWrap>
          <FormWrap>
            <FieldText
              label={'Password'}
              type={'password'}
              value={login.password}
              onChangeValue={(password) => setLogin({ ...login, password })}
              disabled={submited}
            ></FieldText>
          </FormWrap>
          <FormWrap>
            <Small>Forgot your password?</Small>
          </FormWrap>
          <FormWrap>
            <Button label={'Login'} onClick={() => signInWithEmailAndPassword()} disabled={submited}></Button>
          </FormWrap>
        </div>
      </Wrapper>
    </>
  ) : (
    <Wrapper constrain>
      <div>
        <ImageIntro src={MessiIntroImage} alt="intro image"></ImageIntro>
      </div>
      <ModalDiv>
        <div style={{ marginBottom: '34px' }}>
          <Heading size={'28px'}>Discover Your Best <br /> Sport With Us</Heading>
          <Parag>Lorem ipsum dolor sit amet, <br /> consectetur adipiscing elit.</Parag>
        </div>
        <Button label={'Login'} onClick={() => setDisplayLogin(true)}></Button>
      </ModalDiv>
    </Wrapper>
  );


  function signInWithEmailAndPassword(): void {
    setSubmited(true);

    if (login.username.trim().length === 0) {
      setMessages([{ message: 'Username field cannot be empty!', kind: 'warning' }]);
      setSubmited(false);
      return;
    }

    if (login.password.trim().length === 0) {
      setMessages([{ message: 'Password field cannot be empty!', kind: 'warning' }]);
      setSubmited(false);
      return;
    }

    emailAndPassword(login.username, login.password)
      .then(result => {        
        if (result['user']) {
          const data = result.user.providerData[0];          
          const user = data as FirebaseUser;
          console.log(user);
          setUser(user);
          setMessages([{ message: 'Wellcome!', kind: 'success' }]);

          setToken(user.uid); // i know, that should be auth token...

          setTimeout(function () {
            history.push('/main/pick');
          }, 1000);
        }
      })
      .catch(error => {
        console.log(error);
        setMessages([{ message: 'Wrong username or password! Try again.', kind: 'danger' }]);
        setSubmited(false);
      })
  }
}

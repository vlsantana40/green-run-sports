import { ReactElement } from "react";
import { useHistory } from "react-router-dom";
import FieldText from '../../components/FieldText';
import { Wrapper, Heading, Parag, ButtonBack, FormWrap, ImageUser } from "./styled";
import { ArrowLeftIcon } from '../../components/Icons';
import { getUser } from "../../store/useAuth/useAuth";
import DefaultUserImage from '../../assets/images/default-user.png';

export function User(): ReactElement {
    const history = useHistory();
    const user = getUser();

    return (
        <>
            <Wrapper>
                <ButtonBack type="button" onClick={() => history.goBack()}>
                    <ArrowLeftIcon></ArrowLeftIcon>
                </ButtonBack>
                <Heading> User </Heading>
                <Parag>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </Parag>
            </Wrapper>

            <ImageUser>
                <img src={user.photoURL || DefaultUserImage} alt="imageuser"></img>
            </ImageUser>

            <FormWrap>
                <FieldText
                    label={'Name'}
                    type={'text'}
                    value={'Lorem ipsum'}
                    onChangeValue={(value) => console.log(value)}
                    disabled={true}
                ></FieldText>
            </FormWrap>
            <FormWrap>
                <FieldText
                    label={'Email'}
                    type={'text'}
                    value={user.email}
                    onChangeValue={(value) => console.log(value)}
                    disabled={true}
                ></FieldText>
            </FormWrap>
            <FormWrap>
                <FieldText
                    label={'Phone'}
                    type={'text'}
                    value={user.phoneNumber || '55 9999-9999'}
                    onChangeValue={(value) => console.log(value)}
                    disabled={true}
                ></FieldText>
            </FormWrap>
        </>
    );
}
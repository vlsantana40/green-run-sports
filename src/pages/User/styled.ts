
import styled from 'styled-components';

interface HeadingProps {
    size?: string;
}
export const Heading = styled.p<HeadingProps>`
      font-size: ${props => props.size ? props.size : '42px'};
      font-weight: 600;
      margin: 10px 0px 10px;
      color: ${props => props.theme.colorParag};
      font-family: ${props => props.theme.fontBase}
  `;

export const Wrapper = styled.div`
    width: calc(100% - 70px);        
    margin-left: 35px;
    margin-right: 35px;
    padding: 10px 0px;
    letter-spacing: .5px;
`;
export const WrapperList = styled.div`
  width: 100%;        
  padding-left: 35px;
  padding-right: 35px;
  max-height: calc(100vh - 285px);
  overflow-y: auto;
`;

export const Parag = styled.p`
    font-size: 18px;
    font-weight: 400;
    margin-bottom: 10px;
    color: ${props => props.theme.colorSubTitle};
    font-family: ${props => props.theme.fontBase};
  `;

export const ButtonBack = styled.button`
    background-color: transparent;
    border: none;
    outline: none;
    &:focus {
      outline: none;
    }
    >svg {
      width: 30px;
      height: 30px;      
      path {
        stroke: ${props => props.theme.colorParag};
      }
    }
`;

export const FloatAction = styled.div`
    position: absolute;
    right: 19px;
    bottom: 130px;
    z-index: 4;
`;

export const Card = styled.div`
      background-color: ${props => props.theme.colorSecondary};
      border-radius: 12px;
      overflow: hidden;
      margin-bottom: 12px;
      display: flex;
      align-items:center;
      width: 100%;
  
      >div:nth-child(1) {
        width: 75%;
        height: 60px;
        display: flex;
        align-items: center;
        padding: 0px 7px;
        position: relative;

        >p {
          font-size: 20px;
          font-weight: 700;
          color: ${props => props.theme.colorSubTitle};
          font-family: ${props => props.theme.fontBase};
          position: relative;
          z-index: 3;
          display: inline-block;
          text-overflow: ellipsis;
          overflow: hidden;
          white-space: nowrap;
        }
      }
  
      >div:nth-child(2) {
        width: 25%;
        text-align:center;
  
        >button {
          background-color: transparent;
          border: none;        
  
          &:focus {
            outline: none;
          }
  
          >svg {
            width: 23px;
            height: 23px;
            >path {
              fill: ${props => props.theme.colorBody}
            }
          }
        }
      }
  `;


export const FormWrap = styled.div`
  width: calc(100% - 70px);
  margin-left: 35px;
  margin-right: 35px;
  margin-top: 10px;
`;


export const ImageUser = styled.div`
  width: calc(100% - 70px);
  margin-left: 35px;
  margin-right: 35px;
  padding: 20px 0px;

  >img {
      width: 90px;
      height: 90px;
      border-radius: 100%;
      margin: auto;
      display: block;
  }
`;
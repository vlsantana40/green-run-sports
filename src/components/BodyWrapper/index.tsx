import { ReactElement } from 'react';
import styled, { useTheme } from 'styled-components';

const Body = styled.div`
    width:100%;
    min-height: 100vh;
    background-Color: ${props => props.theme.colorBody}
`;

const Div = styled.div`
    min-height: 100vh;
    max-width: 800px;
    width:100%;
    margin:auto;
    position:relative;
    overflow: hidden;
`;

function BodyWrapper({ children }): ReactElement | null {

    const theme = useTheme()
    
    return (
        <Body theme={theme}>
            <Div>{children}</Div>
        </Body>
    );
}


export default BodyWrapper;

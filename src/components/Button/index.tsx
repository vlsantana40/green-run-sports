import { ReactElement } from 'react';
import styled from 'styled-components';

const Btn = styled.button`
    color: #fff;
    font-size: 18px;
    font-weight: 700;
    font-family: ${props => props.theme.fontBase};
    background: linear-gradient(to right,${props => props.theme.colorPrimary},${props => props.theme.colorPrimaryB});
    border: none;
    box-shadow: 0px 4px 30px ${props => props.theme.colorPrimary}cc;
    border-radius: 25px;
    transition: opacity .5s ease;

    &:disabled {
        opacity: .5;
    }
`;

interface ButtonProps {
    label: string;
    onClick: () => void;
    padding?: string;
    disabled?: boolean;
}

function Button(props: ButtonProps): ReactElement | null {
    return (
        <Btn type="button" disabled={props.disabled} style={{ padding: props.padding ? props.padding : '20px 30px' }} onClick={props.onClick}>{props.label}</Btn>
    );
}

export default Button;


import { ReactElement } from 'react';
import styled from 'styled-components';
import TimesIcon from '../../assets/icons/times.svg';
import { AlertPayload } from '../../models/payloads/alert.payload';


interface MessageAlertProps {
    topPosition?: string;
    color?: string;
}
const MessageAlert = styled.div<MessageAlertProps>`
position: absolute;
top: ${props => props.topPosition ? props.topPosition : '20px'};
right: 0px;
z-index: 10;
background-color: ${props => props.color ? props.color : '#ef0f0f'};;
padding: 15px;
display: flex;
align-items:center;
width: calc(100% - 20px);
margin: 0px 10px;
box-shadow: 2px 1px 8px 0px rgba(0,0,0,0.2);
border-radius: 10px;

&>div:nth-child(1) {
   width:80%;

   &>p {
     font-size: 18px;
     font-weight: 400;
     color: #fff;
     font-family: ${props => props.theme.fontBase}
   }
}
&>div:nth-child(2) {
   width:20%;
   text-align: right;
   &>button {
     background-color: #ffffff4d;
     border: none;
     padding: 8px 10px 8px;
     border-radius: 22px;
     &>img {
       width: 20px;
       display:inline-block;
     }
   }
}
`;


interface AlertProps {
    messages: AlertPayload[];
    changed: (value: AlertPayload[]) => void;
}

function Alert(props: AlertProps): ReactElement | null {
    return (
        <>
            {
                props.messages.map((item: AlertPayload, index: number) => {

                    let color = '#ef0f0f';

                    if (item.kind === 'success') {
                        color = '#5cb729'
                    } else if (item.kind === 'warning') {
                        color = '#efba34'
                    }

                    return (
                        <MessageAlert key={index} color={color} topPosition={`${index === 0 ? 20 : (index + 1 * 100)}px`}>
                            <div>
                                <p>{item.message}</p>
                            </div>
                            <div>
                                <button type="button" onClick={() => close(index)}>
                                    <img src={TimesIcon} alt="TimesIcon"></img>
                                </button>
                            </div>
                        </MessageAlert>
                    );
                }


                )
            }
        </>
    );


    function close(index: number): void {
        const clone = JSON.parse(JSON.stringify(props.messages)) as AlertPayload[];
        clone.splice(index, 1);
        props.changed(clone);
    }
}

export default Alert;


import { ReactElement } from 'react';
import styled from 'styled-components';


interface BtnProps {
    primary?: boolean;
    padding?: string;
}
const Btn = styled.button<BtnProps>`
    color: #fff;
    font-size: 18px;
    font-weight: 700;
    padding: ${props => props.padding ? props.padding : '20px 30px'};
    font-family: ${props => props.theme.fontBase};
    background: ${props => props.primary ? `linear-gradient(to right,${props.theme.colorPrimary},${props.theme.colorPrimaryB})` : props.theme.colorSecondary};
    


    border: none;
    box-shadow: 0px 10px 25px ${props => props.primary ? `${props.theme.colorPrimary}14` : '#00000014'};
    border-radius: 100%;
    transition: .5s ease;

    &:disabled {
        opacity: .5;
    }

    &:focus {
        box-shadow: 0px 10px 25px ${props => props.theme.colorPrimary}94;
    }
`;

interface ButtonRoundedProps {
    children: ReactElement;
    onClick: () => void;
    width: string;
    primary: boolean;
    padding?: string;
    disabled?: boolean;
}

function ButtonRounded(props: ButtonRoundedProps): ReactElement | null {
    return (
        <Btn type="button"
            padding={props.padding}
            primary={props.primary}
            disabled={props.disabled}
            onClick={props.onClick}>
            {props.children}
        </Btn>
    );
}

export default ButtonRounded;


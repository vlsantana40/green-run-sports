import { ReactElement } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import styled from 'styled-components';
import DefaultUserImage from '../../assets/images/default-user.png';

const Wrap = styled.div`
    width: calc(100% - 30px);
    padding: 11px;
    margin: 0px 15px;
    border-radius: 24px;
    box-shadow: 1px 1px 6px 0px rgba(0,0,0,0.05);
    background-color: ${props => props.theme.colorSecondary};
    position: absolute;
    bottom: 18px;
    z-index: 99;
    display: flex;
    justify-content: space-between;
    align-items: center;
`;

const Button = styled.button`
    border: none;
    background-color: ${props => props.theme.colorSecondary};
    border-radius: 16px;
    padding: 15px 18px;
    transition: box-shadow .8s ease;

    >svg >path {
        fill: ${props => props.theme.colorBody};
    }

    &:focus {
        box-shadow: 0px 4px 30px ${props => props.theme.colorPrimary}8a;
    }

    &.active {
        background-color: ${props => props.theme.colorBody};

        >svg >path {
            fill: ${props => props.theme.colorPrimary};
        }
    }
`;

const ButtonImage = styled.button`
    border: none;
    background-color: ${props => props.theme.colorSecondary};
    border-radius: 16px;
    padding: 10px 13px 8px;
    transition: box-shadow .8s ease;

    &:focus {
        box-shadow: 0px 4px 30px ${props => props.theme.colorPrimary}8a;
    }

    &.active {
        background-color: ${props => props.theme.colorBody};        
    }

    >img {
        width: 34px;
        height: 34px;
        display: inline-block;
        border-radius: 100%;
    }
`;

function Navigator(): ReactElement | null {

    const history = useHistory();
    const location = useLocation();

    return (
        <Wrap>
            <Button type="button" className={location.pathname === '/main/pick' ? 'active' : ''} onClick={() => history.push('/main/pick')}>
                <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M21.3637 10.1271L21.3636 10.127L13.5505 3.0239C13.2635 2.7611 12.8885 2.61534 12.4994 2.61536C12.1102 2.61537 11.7352 2.76117 11.4483 3.02399L3.63636 10.127C3.47545 10.2735 3.34687 10.452 3.25882 10.6511C3.17078 10.8502 3.1252 11.0654 3.125 11.283V20.3125C3.12547 20.7267 3.29025 21.1239 3.58317 21.4168C3.87609 21.7097 4.27324 21.8745 4.6875 21.875H20.3125C20.7268 21.8745 21.1239 21.7097 21.4168 21.4168C21.7098 21.1239 21.8745 20.7267 21.875 20.3125V11.2831C21.8748 11.0655 21.8292 10.8502 21.7412 10.6512C21.6532 10.4521 21.5246 10.2736 21.3637 10.1271Z" fill="#1A5BE1" />
                </svg>
            </Button>
            <Button type="button" className={location.pathname === '/main/history' ? 'active' : ''} onClick={() => history.push('/main/history')}>
                <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M12.5 3.125C10.6458 3.125 8.83325 3.67483 7.29153 4.70497C5.74982 5.73511 4.54821 7.19929 3.83863 8.91234C3.12906 10.6254 2.94341 12.5104 3.30514 14.329C3.66688 16.1475 4.55976 17.818 5.87088 19.1291C7.182 20.4402 8.85246 21.3331 10.671 21.6949C12.4896 22.0566 14.3746 21.8709 16.0877 21.1614C17.8007 20.4518 19.2649 19.2502 20.295 17.7085C21.3252 16.1668 21.875 14.3542 21.875 12.5C21.8722 10.0145 20.8836 7.63152 19.126 5.87398C17.3685 4.11643 14.9855 3.12781 12.5 3.125ZM16.9194 9.18541L13.0525 13.0524C12.9799 13.125 12.8938 13.1825 12.799 13.2218C12.7042 13.2611 12.6026 13.2813 12.5 13.2813C12.3974 13.2813 12.2958 13.2611 12.2011 13.2218C12.1063 13.1826 12.0201 13.125 11.9476 13.0525C11.875 12.9799 11.8175 12.8938 11.7782 12.799C11.739 12.7042 11.7187 12.6026 11.7187 12.5C11.7187 12.3974 11.7389 12.2958 11.7782 12.2011C11.8175 12.1063 11.875 12.0201 11.9475 11.9476L15.8145 8.08058C15.8871 8.00803 15.9732 7.95048 16.068 7.91122C16.1628 7.87195 16.2644 7.85174 16.367 7.85174C16.4696 7.85174 16.5712 7.87194 16.6659 7.9112C16.7607 7.95046 16.8469 8.00801 16.9194 8.08055C16.992 8.1531 17.0495 8.23922 17.0888 8.33401C17.128 8.42879 17.1482 8.53038 17.1482 8.63298C17.1482 8.73557 17.128 8.83716 17.0888 8.93195C17.0495 9.02674 16.992 9.11286 16.9194 9.18541H16.9194Z" fill="#EDEDED" />
                </svg>
            </Button>
            <Button type="button" className={location.pathname === '/main/notes' ? 'active' : ''} onClick={() => history.push('/main/notes')}>
                <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M19.5312 3.12555H17.9688V2.3443C17.9688 2.1371 17.8864 1.93839 17.7399 1.79187C17.5934 1.64536 17.3947 1.56305 17.1875 1.56305C16.9803 1.56305 16.7816 1.64536 16.6351 1.79187C16.4886 1.93839 16.4062 2.1371 16.4062 2.3443V3.12555H13.2812V2.3443C13.2812 2.1371 13.1989 1.93839 13.0524 1.79187C12.9059 1.64536 12.7072 1.56305 12.5 1.56305C12.2928 1.56305 12.0941 1.64536 11.9476 1.79187C11.8011 1.93839 11.7188 2.1371 11.7188 2.3443V3.12555H8.59375V2.3443C8.59375 2.1371 8.51144 1.93839 8.36493 1.79187C8.21841 1.64536 8.0197 1.56305 7.8125 1.56305C7.6053 1.56305 7.40659 1.64536 7.26007 1.79187C7.11356 1.93839 7.03125 2.1371 7.03125 2.3443V3.12555H5.46875C5.05449 3.12602 4.65734 3.2908 4.36442 3.58372C4.0715 3.87664 3.90672 4.27379 3.90625 4.68805V19.5318C3.9072 20.3603 4.23674 21.1546 4.82259 21.7405C5.40843 22.3263 6.20274 22.6559 7.03125 22.6568H17.9688C18.7973 22.6559 19.5916 22.3263 20.1774 21.7405C20.7633 21.1546 21.0928 20.3603 21.0938 19.5318V4.68805C21.0933 4.27379 20.9285 3.87664 20.6356 3.58372C20.3427 3.2908 19.9455 3.12602 19.5312 3.12555ZM15.625 16.4068H9.375C9.1678 16.4068 8.96909 16.3245 8.82257 16.178C8.67606 16.0315 8.59375 15.8327 8.59375 15.6255C8.59375 15.4183 8.67606 15.2196 8.82257 15.0731C8.96909 14.9266 9.1678 14.8443 9.375 14.8443H15.625C15.8322 14.8443 16.0309 14.9266 16.1774 15.0731C16.3239 15.2196 16.4062 15.4183 16.4062 15.6255C16.4062 15.8327 16.3239 16.0315 16.1774 16.178C16.0309 16.3245 15.8322 16.4068 15.625 16.4068ZM15.625 13.2818H9.375C9.1678 13.2818 8.96909 13.1995 8.82257 13.053C8.67606 12.9065 8.59375 12.7077 8.59375 12.5005C8.59375 12.2933 8.67606 12.0946 8.82257 11.9481C8.96909 11.8016 9.1678 11.7193 9.375 11.7193H15.625C15.8322 11.7193 16.0309 11.8016 16.1774 11.9481C16.3239 12.0946 16.4062 12.2933 16.4062 12.5005C16.4062 12.7077 16.3239 12.9065 16.1774 13.053C16.0309 13.1995 15.8322 13.2818 15.625 13.2818Z" fill="#EDEDED" />
                </svg>
            </Button>
            <ButtonImage type="button" className={location.pathname === '/main/user' ? 'active' : ''} onClick={() => history.push('/main/user')}>
                <img src={DefaultUserImage} alt="defaultUserImage"></img>
            </ButtonImage>
        </Wrap>
    );
}

export default Navigator


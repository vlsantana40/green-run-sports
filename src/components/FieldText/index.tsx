import { ReactElement } from 'react';
import styled from 'styled-components';


const Input = styled.input`
    border: none;
    width: 100%;
    color: ${props => props.theme.colorParag};
    font-family: ${props => props.theme.fontBase};
    font-size: 18px;
    font-weight: 400;
    background-color: transparent;    
    &:focus {
        outline: none;
    }
`;




const Textarea = styled.textarea`
    border: none;
    width: 100%;
    color: ${props => props.theme.colorParag};
    font-family: ${props => props.theme.fontBase};
    font-size: 18px;
    font-weight: 400;
    background-color: transparent;    
    &:focus {
        outline: none;
    }
`;

const Wrap = styled.div`
    transition: opacity .5 ease;
    width: 100%;
    padding: 9px 14px;
    border-radius: 18px;
    background-color: ${props => props.theme.colorSecondary};
`;

const Label = styled.label`    
    font-size: 14px;
    font-weight: 700;
    color: ${props => props.theme.colorLabel};
    font-family: ${props => props.theme.fontBase}
`;

interface FieldTextProps {
    value: string;
    onChangeValue: (value: string) => void;
    name?: string;
    id?: string;
    label?: string;
    type?: string;
    disabled?: boolean;
}

function FieldText(props: FieldTextProps): ReactElement | null {
    return (
        <Wrap style={{ opacity: props.disabled ? '.5' : '1' }}>
            <Label>{props.label}</Label>
            {
                props.type === 'textarea' ? (
                    <Textarea rows={5} disabled={props.disabled} onChange={(event) => props.onChangeValue(event.target.value)} value={props.value}></Textarea>
                ) : (
                    <Input type={props.type ? props.type : 'text'} disabled={props.disabled} onChange={(event) => props.onChangeValue(event.target.value)} value={props.value}></Input>
                )
            }

        </Wrap>

    );
}

export default FieldText

